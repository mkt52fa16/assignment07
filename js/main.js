/*jslint browser: true*/
/*global $, jQuery, alert*/


$(document).ready(function () {
    // pragma
    "use strict";

    $("footer").click(function () {
        $("footer").hide();
    });
    
    $("#car").click(function () {
        $("#car").animate({marginLeft : "+800"}, 1000);
    });
              
    $("p").on("click", function () {
        $("#ghost").fadeToggle(3000);
    });
    
    $("#spring").click(function () {
        $("nav").css("background-color", "#1C4905");
        $("body").css("background-color", "#D8ECC3");
    });
    
    $("#fall").click(function () {
        $("nav").css("background-color", "#D2691E");
        $("body").css("background-color", "#FFF8DC");
    });
});